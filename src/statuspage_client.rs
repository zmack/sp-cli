use console::{Style, Term};
use reqwest::header;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::HashMap;
use url::Url;

use serde_with_macros::skip_serializing_none;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

pub trait Printable {
    fn print(&self);
}

pub struct Client {
    api_key: String,
    base_url: Url,
    http_client: reqwest::Client,
}

type FauxDate = String;

#[derive(Serialize, Deserialize, Debug)]
pub struct FileAttachment {
    size: Option<i32>,
    url: Option<String>,
    original_url: Option<String>,
    normal_url: Option<String>,
    retina_url: Option<String>,
    updated_at: Option<FauxDate>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Page {
    id: String,
    created_at: FauxDate,
    updated_at: FauxDate,
    name: String,
    page_description: Option<String>,
    headline: Option<String>,
    branding: Option<String>,
    subdomain: String,
    domain: Option<String>,
    url: Option<String>,
    support_url: Option<String>,
    hidden_from_search: bool,
    allow_page_subscribers: bool,
    allow_incident_subscribers: bool,
    allow_email_subscribers: bool,
    allow_sms_subscribers: bool,
    allow_rss_atom_feeds: bool,
    allow_webhook_subscribers: bool,
    notifications_from_email: Option<String>,
    notifications_email_footer: Option<String>,
    activity_score: u64,
    twitter_username: Option<String>,
    viewers_must_be_team_members: bool,
    ip_restrictions: Option<String>,
    city: Option<String>,
    state: Option<String>,
    country: Option<String>,
    time_zone: Option<String>,
    css_body_background_color: Option<String>,
    css_font_color: Option<String>,
    css_light_font_color: Option<String>,
    css_greens: Option<String>,
    css_yellows: Option<String>,
    css_oranges: Option<String>,
    css_blues: Option<String>,
    css_reds: Option<String>,
    css_border_color: Option<String>,
    css_graph_color: Option<String>,
    css_link_color: Option<String>,
    favicon_logo: FileAttachment,
    transactional_logo: FileAttachment,
    hero_cover: FileAttachment,
    email_logo: FileAttachment,
    twitter_logo: FileAttachment,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    id: String,
    organization_id: String,
    email: String,
    first_name: Option<String>,
    last_name: Option<String>,
    created_at: FauxDate,
    updated_at: FauxDate,
}

#[skip_serializing_none]
#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Incident {
    id: Option<String>,
    page_id: Option<String>,
    name: Option<String>,
    status: Option<String>,
    created_at: Option<String>,
    updated_at: Option<String>,
    monitoring_at: Option<FauxDate>,
    resolved_at: Option<FauxDate>,
    impact: Option<String>,
    shortlink: Option<String>,
    scheduled_for: Option<FauxDate>,
    scheduled_until: Option<FauxDate>,
    scheduled_remind_prior: Option<bool>,
    scheduled_reminded_at: Option<FauxDate>,
    impact_override: Option<String>,
    scheduled_auto_in_progress: Option<bool>,
    scheduled_auto_completed: Option<bool>,
    started_at: Option<String>,
    incident_updates: Vec<IncidentUpdate>,
    postmortem_body: Option<String>,
    postmortem_body_last_updated_at: Option<FauxDate>,
    postmortem_ignored: Option<bool>,
    postmortem_published_at: Option<FauxDate>,
    postmortem_notified_subscribers: Option<bool>,
    postmortem_notified_twitter: Option<bool>,
    components: Option<Vec<Component>>,
    // metadata: {},
}

#[derive(Serialize, Deserialize, Debug)]
pub struct IncidentUpdate {
    id: String,
    incident_id: String,
    status: String,
    body: String,
    created_at: FauxDate,
    wants_twitter_update: bool,
    twitter_updated_at: Option<FauxDate>,
    updated_at: FauxDate,
    display_at: FauxDate,
    affected_components: Option<Vec<AffectedComponent>>,
    deliver_notifications: bool,
    tweet_id: Option<String>,
    custom_tweet: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Component {
    id: String,
    page_id: String,
    status: String,
    name: String,
    description: Option<String>,
    position: u64,
    group_id: Option<u64>,
    showcase: bool,
    created_at: FauxDate,
    updated_at: FauxDate,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AffectedComponent {
    code: String,
    name: String,
    old_status: String,
    new_status: String,
}

impl Client {
    pub fn new(api_key: &String) -> Client {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            header::AUTHORIZATION,
            header::HeaderValue::from_str(&format!("Oauth {}", api_key)).unwrap(),
        );
        headers.insert(
            header::USER_AGENT,
            header::HeaderValue::from_str(&format!("rust/sp-cli {}", VERSION)).unwrap(),
        );
        headers.insert(
            header::CONTENT_TYPE,
            header::HeaderValue::from_str("application/json").unwrap(),
        );

        Client {
            api_key: api_key.clone(),
            base_url: Url::parse("https://api.statuspage.io/v1/").unwrap(),
            http_client: reqwest::Client::builder()
                .default_headers(headers)
                .build()
                .unwrap(),
        }
    }

    fn incidents_url(&self, page_code: &String) -> Url {
        self.base_url
            .join(&format!("pages/{}/incidents", page_code))
            .unwrap()
    }

    fn incident_url(&self, page_code: &String, incident_code: &String) -> Url {
        self.base_url
            .join(&format!(
                "pages/{}/incidents/{}.json",
                page_code, incident_code
            ))
            .unwrap()
    }

    fn components_url(&self, page_code: &String) -> Url {
        self.base_url
            .join(&format!("pages/{}/components.json", page_code))
            .unwrap()
    }

    fn component_url(&self, page_code: &String, component_code: &String) -> Url {
        self.base_url
            .join(&format!(
                "pages/{}/components/{}.json",
                page_code, component_code
            ))
            .unwrap()
    }

    fn users_url(&self, organization_code: &String) -> Url {
        self.base_url
            .join(&format!("organizations/{}/users.json", organization_code))
            .unwrap()
    }

    fn pages_url(&self) -> Url {
        self.base_url.join(&"pages.json").unwrap()
    }

    fn page_url(&self, page_code: &String) -> Url {
        self.base_url
            .join(&format!("pages/{}.json", page_code))
            .unwrap()
    }

    pub fn get_incidents(
        &self,
        page_code: &String,
    ) -> Result<Vec<Incident>, Box<dyn std::error::Error>> {
        let response = self.http_client.get(self.incidents_url(page_code)).send();
        let resp: Vec<Incident> = response?.json()?;

        return Ok(resp);
    }

    pub fn get_incident(
        &self,
        page_code: &String,
        incident_code: &String,
    ) -> Result<Incident, Box<dyn std::error::Error>> {
        let response = self
            .http_client
            .get(self.incident_url(page_code, incident_code))
            .send();
        let incident: Incident = response?.json()?;

        return Ok(incident);
    }

    pub fn create_incident(
        &self,
        page_code: &String,
        name: &String,
    ) -> Result<Incident, Box<dyn std::error::Error>> {
        let incident = Incident {
            name: Some(name.to_string()),
            ..Default::default()
        };
        let json_payload = json!({ "incident": incident }).to_string();
        let response = self
            .http_client
            .post(self.incidents_url(page_code))
            .body(json_payload.clone())
            .send();
        let created_incident: Incident = response?.json()?;

        return Ok(created_incident);
    }

    pub fn update_incident(
        &self,
        page_code: &String,
        incident_code: &String,
        status: &Option<&str>,
        body: &Option<&str>
    ) -> Result<Incident, Box<dyn std::error::Error>> {
        let incident_json = json!({
                "status": status.map(|s| String::from(s)),
                "body": body.map(|s| String::from(s))
        });
        let json_payload = json!({
            "incident": incident_json
        }).to_string();

        let response = self
            .http_client
            .patch(self.incident_url(page_code, incident_code))
            .body(json_payload.clone())
            .send();

        let updated_incident: Incident = response?.json()?;

        return Ok(updated_incident)
    }

    pub fn get_components(
        &self,
        page_code: &String,
    ) -> Result<Vec<Component>, Box<dyn std::error::Error>> {
        let response = self.http_client.get(self.components_url(page_code)).send();
        let components: Vec<Component> = response?.json()?;

        return Ok(components);
    }

    pub fn get_component(
        &self,
        page_code: &String,
        component_code: &String,
    ) -> Result<Component, Box<dyn std::error::Error>> {
        let response = self
            .http_client
            .get(self.component_url(page_code, component_code))
            .send();
        let component: Component = response?.json()?;

        return Ok(component);
    }

    pub fn create_component(
        &self,
        page_code: &String,
        component_fields: HashMap<String, HashMap<String, String>>,
    ) -> Result<Component, Box<dyn std::error::Error>> {
        let serialized_fields = serde_json::to_string(&component_fields)?;
        let response = self
            .http_client
            .post(self.components_url(page_code))
            .body(serialized_fields)
            .send();
        let component: Component = response?.json()?;
        return Ok(component);
    }

    pub fn get_users(
        &self,
        organization_code: &String,
    ) -> Result<Vec<User>, Box<dyn std::error::Error>> {
        let response = self
            .http_client
            .get(self.users_url(organization_code))
            .send();
        let users: Vec<User> = response?.json()?;

        return Ok(users);
    }

    pub fn get_pages(&self) -> Result<Vec<Page>, Box<dyn std::error::Error>> {
        let response = self.http_client.get(self.pages_url()).send();
        let pages: Vec<Page> = response?.json()?;

        return Ok(pages);
    }

    pub fn get_page(&self, page_code: &String) -> Result<Page, Box<dyn std::error::Error>> {
        let response = self.http_client.get(self.page_url(page_code)).send();
        let page: Page = response?.json()?;

        return Ok(page);
    }
}

fn style_for_status(status: &String) -> Style {
    match status.as_str() {
        "operational" => Style::new().green(),
        "degraded" => Style::new().yellow(),
        "major_outage" => Style::new().red(),
        _ => Style::new(),
    }
}

fn styled_status(status: &String) -> String {
    style_for_status(status).apply_to(status).to_string()
}

impl Printable for Component {
    fn print(&self) {
        let label = Style::new().green();
        let position = Style::new().yellow();
        println!(
            "{:>#20} [{}] {}",
            label.apply_to("Name"),
            position.apply_to(self.position),
            self.name
        );
        println!("{:>#20} {}", label.apply_to("Id"), self.id);
        println!(
            "{:>#20} {}",
            label.apply_to("Status"),
            styled_status(&self.status)
        );

        if let Some(ref description) = self.description {
            println!("{:>#20} {:?}", label.apply_to("Description"), description);
        }
        println!()
    }
}

impl Printable for Page {
    fn print(&self) {
        println!("{:#?}", self)
    }
}

impl Printable for User {
    fn print(&self) {
        println!("{:#?}", self)
    }
}

impl Printable for Incident {
    fn print(&self) {
        println!("{:#?}", self)
    }
}

impl Printable for IncidentUpdate {
    fn print(&self) {
        println!("{:#?}", self)
    }
}
