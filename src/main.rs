extern crate clap;
extern crate console;
extern crate dirs;
extern crate reqwest;
extern crate serde;
extern crate serde_json;
extern crate serde_with_macros;
extern crate toml;
extern crate url;

mod configuration;
mod statuspage_client;

use clap::{App, Arg, SubCommand};
use std::collections::HashMap;

fn print<T, E>(printable: Result<T, E>)
where
    T: statuspage_client::Printable,
{
    printable.ok().unwrap().print();
}

fn print_list<T, E>(printables: Result<Vec<T>, E>)
where
    T: statuspage_client::Printable,
{
    for printable in printables.ok().unwrap() {
        printable.print();
    }
}

fn main() {
    let matches = App::new("Statuspage CLI")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Makes requests the the Statuspage API")
        .subcommand(SubCommand::with_name("incident:list").about("List all incidents"))
        .subcommand(
            SubCommand::with_name("incident:show")
                .arg(
                    Arg::with_name("id")
                        .help("The id of the incident")
                        .required(true),
                )
                .about("Display an incident"),
        )
        .subcommand(
            SubCommand::with_name("incident:create")
                .arg(
                    Arg::with_name("incident_name")
                        .help("Name of the incident")
                        .required(true),
                )
                .about("Create an incident"),
        )
        .subcommand(
            SubCommand::with_name("incident:update")
                .arg(
                    Arg::with_name("id")
                        .help("Incident Id")
                        .required(true),
                )
                .arg(
                    Arg::with_name("status")
                        .help("New incident status")
                        .required(false),
                )
                .arg(
                    Arg::with_name("body")
                        .help("Incident update body")
                        .required(false),
                )
                .about("Create an incident"),
        )
        .subcommand(SubCommand::with_name("component:list").about("List all components"))
        .subcommand(
            SubCommand::with_name("component:show")
                .arg(
                    Arg::with_name("component_id")
                        .help("The id of the component")
                        .required(true),
                )
                .about("Display a component"),
        )
        .subcommand(SubCommand::with_name("user:list").about("List all users"))
        .subcommand(SubCommand::with_name("page:list").about("List all pages"))
        .subcommand(
            SubCommand::with_name("page:show")
                .arg(
                    Arg::with_name("page_id")
                        .help("The id of the page")
                        .required(true),
                )
                .about("Display a page"),
        )
        .subcommand(
            SubCommand::with_name("test")
                .arg(
                    Arg::with_name("page_id")
                        .help("The id of the page")
                        .required(false),
                )
                .arg(
                    Arg::with_name("name")
                        .long("name")
                        .help("The id of the page")
                        .required(true),
                )
                .arg(
                    Arg::with_name("showcase")
                        .long("showcase")
                        .help("The id of the page")
                        .required(true),
                )
                .arg(
                    Arg::with_name("description")
                        .long("description")
                        .help("The id of the page")
                        .required(true),
                )
                .about("Kick the can"),
        )
        .get_matches();

    let config = configuration::read().unwrap();
    let client = statuspage_client::Client::new(&config.api_key);

    if let Some(_matches) = matches.subcommand_matches("incident:list") {
        print_list(client.get_incidents(&config.page_code))
    } else if let Some(matches) = matches.subcommand_matches("incident:show") {
        let incident_id = String::from(matches.value_of("id").unwrap());
        print(client.get_incident(&config.page_code, &incident_id))
    } else if let Some(matches) = matches.subcommand_matches("incident:create") {
        let incident_name = String::from(matches.value_of("incident_name").unwrap());
        print(client.create_incident(&config.page_code, &incident_name))
    } else if let Some(matches) = matches.subcommand_matches("incident:update") {
        let incident_code = String::from(matches.value_of("id").unwrap());
        let incident_status = matches.value_of("status");
        let incident_body = matches.value_of("body");
        print(client.update_incident(&config.page_code, &incident_code, &incident_status, &incident_body))
    } else if let Some(_matches) = matches.subcommand_matches("component:list") {
        print_list(client.get_components(&config.page_code))
    } else if let Some(matches) = matches.subcommand_matches("component:show") {
        let component_id = String::from(matches.value_of("component_id").unwrap());
        print(client.get_component(&config.page_code, &component_id))
    } else if let Some(_matches) = matches.subcommand_matches("user:list") {
        print_list(client.get_users(&config.organization_code))
    } else if let Some(_matches) = matches.subcommand_matches("page:list") {
        print_list(client.get_pages())
    } else if let Some(matches) = matches.subcommand_matches("page:show") {
        let page_id = String::from(matches.value_of("page_id").unwrap());
        print(client.get_page(&page_id))
    } else if let Some(matches) = matches.subcommand_matches("test") {
        let mut component_params = HashMap::new();

        component_params.insert("name".to_string(), "An amazing component".to_string());
        component_params.insert("showcase".to_string(), "false".to_string());

        component_params.insert(
            "description".to_string(),
            "Things are described".to_string(),
        );

        let mut params = HashMap::new();
        params.insert("component".to_string(), component_params);

        let result = client.create_component(&config.page_code, params);

        println!("{:?}", result)
    }
}
