use serde::{Deserialize, Serialize};
use std::{env, fs};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub api_key: String,
    pub page_code: String,
    pub organization_code: String,
}

pub fn read() -> Result<Config, Box<dyn std::error::Error>> {
    let path = dirs::home_dir()
        .unwrap()
        .join(".config")
        .join("sp-cli")
        .join("config.toml");
    let bytes = fs::read(path)?;
    let contents = String::from_utf8_lossy(&bytes);

    Ok(toml::from_str(&contents)?)
}
