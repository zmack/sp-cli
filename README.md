## What is this

sp-cli is a commandline tool that interacts with the Statuspage API. It's meant to be easy to use, fully-featured and pretty.

## Configuration

sp-cli looks for its configuration in `~/.config/sp-cli/config.toml`. The expectation is that this file provides an api token and a page code.
Currently the application will not work if this file does not exist.

The format is as follows:

```
api_key = 'my_api_token'
page_code = 'my_page_code'
```

## Features

This is what we're shooting for here. Some of these are done, others are planned

- [ ] Listing incidents
- [ ] Listing components
- [ ] Listing users
- [ ] Listing pages
- [ ] Listing page access users
- [ ] Listing page access groups
- [ ] Listing incident templates
- [ ] Creating / Updating incidents
- [ ] Creating / Updating components
- [ ] Creating users
- [ ] Updating pages
- [ ] Creating / Updating page access users
- [ ] Creating / Updating page access groups
- [ ] Creating / Updating incident templates
- [ ] Creating entities from piped-in JSON arrays
- [ ] Creating entities across profiles
- [ ] Multiple profiles
- [ ] Automatic setup / caching for profiles
